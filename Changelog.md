# CHANGELOG

## v1.1.0
- Added Dark Mode Support
- Fixed header styling

## v1.0.0
- First version
- Basic dividend calculation function