import {BrowserRouter, Routes, Route} from "react-router-dom";

import Layout from '../components/core/layout';
import Home from '../pages/home';

const Routers = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Layout />}>
                    <Route path="/" element={<Home />} />
                </Route>
            </Routes>
        </BrowserRouter>
    )
}
export default Routers;
