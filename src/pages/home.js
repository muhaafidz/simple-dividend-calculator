import { useContext, useEffect } from 'react';
import { FundOutlined } from '@ant-design/icons'
import { Card, Tag, Progress, Table, Button, Radio, Form, InputNumber, Select } from 'antd'
import { CalculationContext } from '../components/context';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import './scss/home.scss'

const { Option } = Select;

const InputForm = () => {
    const {currency, setCurrency, setResult} = useContext(CalculationContext)

    const numberFormatter = (value) => {
        return `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    }
    const initialValues = {
        currency: "RM",
        initialSaving: 0,
        monthlyContribution: 0,
        dividend: 0,
        age: 0,
        projection: 5,
    }
    const onFinish = values => {
        setResult(values)
    }
    return (
        <div className="form-wrapper">
            <Card>
                <Form
                name="simple-form"
                labelCol={{ span: 8 }}
                wrapperCol={{ span: 16 }}
                initialValues={initialValues}
                onFinish={onFinish}
                requiredMark='optional'
                >
                    <Form.Item
                        label="Currency"
                        name="currency"
                        rules={[{ required: true, message: 'Please input currency!' }]}
                        >
                        <Radio.Group buttonStyle="solid" onChange={(e) => setCurrency(e.target.value)}>
                            <Radio.Button value="RM">RM</Radio.Button>
                            <Radio.Button value="$">$</Radio.Button>
                            <Radio.Button value="€">€</Radio.Button>
                            <Radio.Button value="£">£</Radio.Button>
                            <Radio.Button value="¥">¥</Radio.Button>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item
                        label="Initial Saving"
                        name="initialSaving"
                        rules={[{ required: true, message: 'Please input initial saving!' }]}
                        >
                        <InputNumber prefix={currency} min={0} style={{ width: "100%", maxWidth: "400px" }} formatter={value => numberFormatter(value)} />
                    </Form.Item>
                    <Form.Item
                        label="Monthly Contribution"
                        name="monthlyContribution"
                        rules={[{ required: true, message: 'Please input monthly contribution!' }]}
                        >
                        <InputNumber prefix={currency} min={0} style={{ width: "100%", maxWidth: "400px" }} formatter={value => numberFormatter(value)} />
                    </Form.Item>
                    <Form.Item
                        label="Dividend (Yearly)"
                        name="dividend"
                        rules={[{ required: true, message: 'Please input yearly dividend!' }]}
                        >
                        <InputNumber prefix="%" min={0} style={{ width: "100%", maxWidth: "400px" }} />
                    </Form.Item>
                    <Form.Item
                        label="Current Age"
                        name="age"
                        rules={[{ required: true, message: 'Please input your current age!' }]}
                        >
                        <InputNumber min={0} style={{ width: "100%", maxWidth: "400px" }} />
                    </Form.Item>
                    <Form.Item
                        label="Projection (Years)"
                        name="projection"
                        rules={[{ required: true, message: 'Please select projection in years!' }]}
                        >
                        <Radio.Group buttonStyle="solid">
                            <Radio.Button value={5}>5</Radio.Button>
                            <Radio.Button value={10}>10</Radio.Button>
                            <Radio.Button value={20}>20</Radio.Button>
                            <Radio.Button value={30}>30</Radio.Button>
                            <Radio.Button value={40}>40</Radio.Button>
                            <Radio.Button value={50}>50</Radio.Button>
                            <Radio.Button value={75}>75</Radio.Button>
                            <Radio.Button value={100}>100</Radio.Button>
                        </Radio.Group>
                    </Form.Item>
                    <Form.Item className="button-container">
                        <Button type="primary" htmlType="submit" className="button-calculate">
                            Calculate
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </div>
    )
}

const FormatNumber = (num) => {
    return Number(num).toLocaleString(undefined, { minimumFractionDigits: 2, maximumFractionDigits: 2 });
}

const CalculateProjection = () => {
    const { result, setGraphData, currency } = useContext(CalculationContext)
    let dataSource = []
    let countNormalSaving = 0;
    let countDividendSaving = 0;
    for(let i=0;i<=result.projection;i++) {
        let profit = 0
        if (i===0) {
            countNormalSaving = result.initialSaving
            countDividendSaving = result.initialSaving
        }
        else {
            let yearlyContribution = result.monthlyContribution * 12
            let initialDividendSaving = yearlyContribution + countDividendSaving
            profit = initialDividendSaving * result.dividend / 100
            countNormalSaving += yearlyContribution
            countDividendSaving = profit + initialDividendSaving
        }
        let percentageDifference = (countDividendSaving - countNormalSaving) / countNormalSaving * 100
        let row = {
            key: i,
            year: i,
            age: result.age + i,
            dividend: profit,
            normalSaving: countNormalSaving,
            dividendSaving: countDividendSaving,
            differences: (countDividendSaving - countNormalSaving),
            percentageDifference: percentageDifference,
            dividendDisplay: `${currency} ${FormatNumber(profit)}`,
            normalSavingDisplay: `${currency} ${FormatNumber(countNormalSaving)}`,
            dividendSavingDisplay: `${currency} ${FormatNumber(countDividendSaving)}`,
            differencesDisplay: `${currency} ${FormatNumber(countDividendSaving - countNormalSaving)} (+${FormatNumber(percentageDifference)}%)`,
            percentageDifferenceDisplay: `${FormatNumber(percentageDifference)}`,

        }
        dataSource.push(row)
    }

    useEffect(() => {setGraphData(dataSource)}
    ,[result]);

    return dataSource
}

const Projection = () => {
    let dataSource = CalculateProjection()
    let percentSaving = 100
    let percentDividend = 100
    if (dataSource.length > 0) {
        let lastTotalSaving = parseFloat(dataSource[dataSource.length-1].normalSaving)
        let lastTotalDividend = parseFloat(dataSource[dataSource.length-1].dividendSaving)
        // console.log('final1', dataSource[dataSource.length-1].normalSaving, dataSource[dataSource.length-1].dividendSaving)
        // console.log('final2', lastTotalSaving, lastTotalDividend)
        if(lastTotalDividend>lastTotalSaving) {
            percentSaving = lastTotalSaving / lastTotalDividend * 100
        } else {
            percentDividend = lastTotalDividend / lastTotalSaving * 100
        }
    }
    // console.log('percent', percentSaving, percentDividend)
    const columns = [
        {
          title: 'Year',
          dataIndex: 'year',
          key: 'year',
          fixed: 'left',
          width: 60,
        },
        {
          title: 'Age',
          dataIndex: 'age',
          key: 'age',
          width: 60,
        },
        {
          title: 'Dividend',
          dataIndex: 'dividendDisplay',
          key: 'dividend',
        },
        {
          title: 'Normal Saving',
          dataIndex: 'normalSavingDisplay',
          key: 'normalSaving',
        },
        {
          title: 'Dividend Saving',
          dataIndex: 'dividendSavingDisplay',
          key: 'dividendSaving',
        },
        {
          title: 'Differences',
          dataIndex: 'differencesDisplay',
          key: 'differences',
          render: tag => (
            <Tag color={"green"}>
                {tag}
            </Tag>
          ),
        },
      ];

    return (
        <>
            {dataSource ?
                <>
                    <div className="line-compare-wrapper">

                        <h4>Normal Saving</h4>
                        <Progress
                        strokeColor={{
                            from: '#3f4c6b',
                            to: '#606c88',
                        }}
                        percent={FormatNumber(percentSaving)}
                        status="active"
                        />
                        <h4>Dividend Saving</h4>
                        <Progress
                        strokeColor={{
                            from: '#108ee9',
                            to: '#87d068',
                        }}
                        percent={FormatNumber(percentDividend)}
                        status="active"
                        />
                    </div>
                </>
                :
                <></>
            }

            <ResponsiveContainer width="100%" height="100%">
                <LineChart
                    width={500}
                    height={300}
                    data={dataSource}
                    margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}
                >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="age" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey="normalSaving" stroke="#8884d8" activeDot={{ r: 8 }} />
                <Line type="monotone" dataKey="dividendSaving" stroke="#82ca9d" />
                </LineChart>
            </ResponsiveContainer>

            <Table dataSource={dataSource} columns={columns} scroll={{ x: 1000 }} size="small" />
        </>
    )
}

const Result = () => {
    const { result } = useContext(CalculationContext)
    return (
        <>
            {result ?
                <Projection />
                :
                <FundOutlined style={{fontSize:"50px", textAlign: "center", display: "block", color: "#444444"}} />
            }
        </>
    )
}

const Home = () => {
    

    return (
        <section id="section-home">
            <div className="container">
                <InputForm />
                <Result />
            </div>
        </section>
    )
}

export default Home