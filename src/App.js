import logo from './logo.svg';
import './App.scss';
import Routers from './routers';
import { CalculationContext, Context } from './components/context';
import { useCallback, useContext, useEffect } from 'react';

function App() {
  const { darkMode, setDarkMode } = useContext(CalculationContext)
  // Dark mode theme on body
  useEffect(() => {
    console.log('DM', darkMode, typeof(darkMode))
    darkMode ?
      document.body.classList.add('dark') :
      document.body.classList.remove('dark');
  }, [darkMode])

  return (
    <div className='App'>
      <Routers />
    </div>
  );
}

export default App;
