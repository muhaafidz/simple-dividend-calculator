import { Link } from 'react-router-dom'
import { Switch } from 'antd'
import { FaMoon, FaSun } from "react-icons/fa";
import './scss/header.scss'
import logo from '../../assets/img/sdc-logo.png'
import { useContext } from 'react';
import { CalculationContext } from '../context';

const Header = () => {
    const { darkMode, setDarkMode } = useContext(CalculationContext)
    console.log(darkMode)
    const toggleDarkMode = (e) => {
        setDarkMode(e)
        localStorage.setItem('darkTheme', e);
    }
    const isDarkMode = () => {
        let darkData = localStorage.getItem('darkTheme') === 'true' ? true : false;
        setDarkMode(darkData)
        return darkData;
    }


    return (
        <header id="header">
            <div className="container">
                <div className="nav-wrapper">
                    <div className="nav-logo">
                        <img src={logo} />   
                    </div> 
                    <div className="nav-brand">
                        Simple Dividend Calculator    
                    </div> 
                    <div className="nav-items">
                        <div className="nav-link">
                            {/* <Link to="/">Home</Link> */}
                            <Switch
                                checkedChildren={<FaMoon />}
                                unCheckedChildren={<FaSun />}
                                defaultChecked={() => isDarkMode()}
                                onChange={(e) => toggleDarkMode(e)}
                            />
                        </div>
                    </div>
                </div> 
            </div> 
        </header>
    )
}

export default Header