import './scss/footer.scss'

const Footer = () => {
  let fullYear = new Date().getFullYear();
    return (
        <footer>
          <p className="version">v1.1.0</p>
          <p>©{fullYear}. Made by <strong>MUHAAFIDZ</strong></p>
        </footer>
    )
}

export default Footer