import { createContext, useMemo, useState } from "react";

const CalculationContext = createContext(null)

const Context = ({children})=> {
    const [currency, setCurrency] = useState('RM');
    const [graphData, setGraphData] = useState([]);
    const [result, setResult] = useState({});
    const [darkMode, setDarkMode] = useState(false);

    const contextList = useMemo(() => ({
            currency,
            graphData,
            result,
            darkMode,
            setCurrency,
            setGraphData,
            setResult,
            setDarkMode
        }), [
            currency,
            graphData,
            result,
            darkMode,
            setCurrency,
            setGraphData,
            setResult,
            setDarkMode
        ])
        
    return (
        <CalculationContext.Provider value={contextList}>
            {children}
        </CalculationContext.Provider>
    )
}

export { Context, CalculationContext }